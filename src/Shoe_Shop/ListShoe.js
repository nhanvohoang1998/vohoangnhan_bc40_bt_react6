import React from 'react'
import ItemShoe from './ItemShoe'

export default function ListShoe(props) {
  let {list} = props
  console.log(list)
  return (
    <div class="row">
        {list.map((shoe, index)=>{
            return <ItemShoe key={index} handleOnclick={props.handleAddToCart} item={shoe}/>
        })}
      </div>
  )
}
