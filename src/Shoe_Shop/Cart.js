import React from 'react'

export default function Cart(props) {
  let renderTbody = () => {
    return props.cart.map((item) => {
      return (
        <tr>
          <td>{item.id}</td>
          <td>{item.name}</td>
          <td>
            <button onClick={()=>{props.handleChangeQuantity(false,item)}} className='btn btn-danger'>-</button>
            <strong className='mx-3'>{item.soLuong}</strong>
            <button onClick={()=>{props.handleChangeQuantity(true,item)}} className='btn btn-success'>+</button>
          </td>
          <td>{item.price * item.soLuong}</td>
          <td><img src={item.image} style={{ width: 50 }} alt="" /></td>
          <td>
            <button onClick={() => {props.handleRemoveItem(item) }} className='btn btn-warning'>Xóa</button>
          </td>
        </tr>
      )
    })

  }
  return (
    <div>
      <table className='table'>
        <thead>
          <th>ID</th>
          <th>Name</th>
          <th>Quantity</th>
          <th>Price</th>
          <th>Img</th>
          <th>Option</th>
        </thead>
        <tbody>
          {renderTbody()}
        </tbody>
      </table>
    </div>
  )
}
