import React, { useState } from 'react'
import ListShoe from './ListShoe'
import Cart from './Cart'
import { dataShoe } from './data_shoe'

export default function Ex_Shoe_Shop() {
  const [listShoe, setListShoe] = useState(dataShoe)
  const [cart, setCart] = useState([])

  let handleAddToCart = (shoe) => {
    let cloneCart = [...cart];
    let index = cloneCart.findIndex((item) => {
      return (shoe.id == item.id)
    })
    //th1: sp chư có trong giở hàng => push
    if (index == -1) {
      let newShoe = { ...shoe, soLuong: 1 }
      cloneCart.push(newShoe)
    } else {
      cloneCart[index].soLuong += 1;
    }
    //th2: sp đã có trong giở hàng => update số lượng

    setCart(cloneCart)
  }

  let handleRemoveItem =(item)=>{
    let cloneCart = [...cart]
    let index = cloneCart.findIndex((shoe)=>{
      return item.id === shoe.id
    })
    cloneCart.splice(index,1)
    setCart(cloneCart)
  }

  let handleChangeQuantity = (check,shoe) => { 
    let cloneCart = [...cart]
    let index = cloneCart.findIndex((item)=>{
      return item.id === shoe.id
    })
    if(check){
      cloneCart[index].soLuong+=1;
    }else{
      cloneCart[index].soLuong-=1;
      if(cloneCart[index].soLuong==0){
        cloneCart.splice(index,1)
      }
    }
    setCart(cloneCart)
   }

  return (
    <div className='container'>
      <Cart handleChangeQuantity={handleChangeQuantity} handleRemoveItem={handleRemoveItem} cart={cart} />
      <ListShoe handleAddToCart={handleAddToCart} list={listShoe} />
    </div>
  )
}
