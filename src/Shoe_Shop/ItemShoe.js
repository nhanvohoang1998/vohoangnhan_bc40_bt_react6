import React from 'react'

export default function ItemShoe(props) {
  let {image,name,price}= props.item
  console.log("test", props)
  console.log("imga", image)
  return (
    <div className='col-3 p-4'>
                <div class="card">
                    <img class="card-img-top" src={image} alt="Card image cap" />
                    <div class="card-body">
                        <h5 class="card-title">{name}</h5>
                        <p class="card-text">{price}</p>
                        <a onClick={()=>{props.handleOnclick(props.item)}} href="#" class="btn btn-primary">Add to card</a>
                    </div>
                </div>
            </div>
  )
}
